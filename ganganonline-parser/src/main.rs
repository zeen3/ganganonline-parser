use ganganonline_parser_lib::parse_ganganonline_proto;
use serde_json::to_string;
use std::{
	error::Error,
	fmt,
	io::{
		stdin,
		Read,
	},
};

#[derive(Debug)]
enum Failure {
	Io(::std::io::Error),
	Proto(::quick_protobuf::Error),
	Json(::serde_json::Error),
}
impl From<::std::io::Error> for Failure {
	fn from(io: ::std::io::Error) -> Self {
		Failure::Io(io)
	}
}
impl From<::quick_protobuf::Error> for Failure {
	fn from(qpb: ::quick_protobuf::Error) -> Self {
		Failure::Proto(qpb)
	}
}
impl From<::serde_json::Error> for Failure {
	fn from(ser: ::serde_json::Error) -> Self {
		Failure::Json(ser)
	}
}
impl fmt::Display for Failure {
	fn fmt(
		&self,
		fmt: &mut fmt::Formatter,
	) -> fmt::Result {
		fmt::Debug::fmt(self, fmt)
	}
}
impl Error for Failure {}
fn main() -> Result<(), Failure> {
	let mut data = Vec::with_capacity(0xffff);
	let len = stdin().read_to_end(&mut data)?;
	print!("{}", to_string(&parse_ganganonline_proto(&data[..len])?)?);
	Ok(())
}
