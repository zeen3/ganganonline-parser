# ganganonline-parser-lib

- in binary (`cargo install ganganonline-parser`, `ganganonline-parser < gangan_data.bin`): parses the output (stdin) of ganganonline data and re-outputs as JSON for use with tools such as jq.
- in lib: offers a singular function to decode a slice, and a module for use with objects.

Build requirements:

- cargo

Recommended setup:

- install the musl stuff (`git clone git://git.musl-libc.org/musl`, follow INSTALL)
- install the musl toolchain (`rustup target add x86_64-unknown-linux-musl`)
- run `cargo build --release --target x86_64-unknown-linux-musl`
- run `cp target/x86_64-unknown-linux-musl/release/ganganonline-parser ~/.local/bin` if you need that

Recommended installs:

- [`jq`](https://stedolan.github.io/jq/)

Prebuilt binaries:

yeah I need to figure that out

Usage:

```shell
curl --http2-prior-knowledge -d '' 'https://web-ggo.tokyo-cdn.com/{}' | ganganonline-parser-json | jq .
```
